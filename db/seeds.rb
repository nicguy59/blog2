# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

1.upto(10) do |number|
  article = Article.new(title: 'Title' + number.to_s,
                        text: 'Text' + number.to_s)
  article.save

  1.upto(4) do |num|
    comment = Comment.new(commenter: 'Commenter' + num.to_s,
                          body: 'Body' + num.to_s)
    comment.save
    article.comments << comment
  end
end
